import com.google.gson.Gson;
import parser.InputParser;
import parser.TypeParser;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class Meta {
    public String methodName;
    public String returnType;
    public String[] paramTypes;
    public int target;
}

class Test {
    public String[] input;
    public String expected;
}

public class TestRunner {
    public static void run(Class<?> clazz, String metaJson, String testsJSon)
            throws NoSuchMethodException,
            InvocationTargetException,
            IllegalAccessException,
            InstantiationException, ClassNotFoundException {

        Gson gson = new Gson();
        Meta meta = gson.fromJson(metaJson, Meta.class);
        Test[] tests = gson.fromJson(testsJSon, Test[].class);

        String methodName = meta.methodName;
        String[] paramTypes = meta.paramTypes;

        Method method = getMethod(clazz, methodName, paramTypes);

        for (Test test : tests) {
            runTest(clazz, paramTypes, method, test);
        }
    }

    private static void runTest(Class<?> clazz, String[] paramTypes, Method method, Test test) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        int nArgs = test.input.length;
        Object[] args = new Object[nArgs];

        for (int i = 0; i < nArgs; i++) {
            String input = test.input[i];
            args[i] = InputParser.parse(input, paramTypes[i]);
        }
        Object result = method.invoke(clazz.newInstance(), args);
        System.out.println(result);
    }

    private static Method getMethod(Class<?> clazz, String methodName, String[] paramTypes) throws NoSuchMethodException {
        Class<?>[] types = new Class<?>[paramTypes.length];
        for (int i = 0; i < paramTypes.length; i++) {
            types[i] = TypeParser.parse(paramTypes[i]);
        }

        return clazz.getDeclaredMethod(methodName, types);
    }
}
