package parser;

import java.util.HashMap;
import java.util.Map;

public class TypeParser {
    private static Map<String, Class<?>> typeMapping;

    static {
        typeMapping = new HashMap<>();
        typeMapping.put("int", int.class);
        typeMapping.put("int[]", int[].class);
    }

    public static Class<?> parse(String type) {
        return typeMapping.get(type);
    }
}
