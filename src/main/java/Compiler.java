import java.io.*;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.tools.*;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;

public class Compiler {
    private static JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();;

    public Compiler() {}

    public static Class<?> compile(String className, String source) throws Exception {
        JavaSourceFromString sourcecode = new JavaSourceFromString(className, source);
        JavaCompiledCode javaCompiledCode = new JavaCompiledCode(className);
        MemJavaFileManager fileManager = new MemJavaFileManager(compiler.getStandardFileManager(null, null, null), javaCompiledCode);
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();

        Iterable<? extends JavaFileObject> compilationUnits = Arrays.asList(sourcecode);
        CompilationTask task = compiler.getTask(null, fileManager, diagnostics, null, null, compilationUnits);

        boolean success = task.call();
        for (Diagnostic diagnostic : diagnostics.getDiagnostics()) {
            System.out.println(diagnostic.getCode());
            System.out.println(diagnostic.getKind());
            System.out.println(diagnostic.getPosition());
            System.out.println(diagnostic.getStartPosition());
            System.out.println(diagnostic.getEndPosition());
            System.out.println(diagnostic.getSource());
            System.out.println(diagnostic.getMessage(null));
        }

        if (success) {
            MemClassLoader memClassLoader = new MemClassLoader();
            memClassLoader.setCode(javaCompiledCode);
            return memClassLoader.findClass(className);
        }
        return null;
    }
}

class JavaSourceFromString extends SimpleJavaFileObject {
    final String code;

    protected JavaSourceFromString(String name, String code) {
        super(URI.create("string:///" + name.replace('.', '/') + Kind.SOURCE.extension),
                Kind.SOURCE);
        this.code = code;
    }

    @Override
    public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
        return this.code;
    }
}

class JavaCompiledCode extends SimpleJavaFileObject {
    private ByteArrayOutputStream baos = new ByteArrayOutputStream();

    public JavaCompiledCode(String className) throws Exception {
        super(new URI(className), Kind.CLASS);
    }

    @Override
    public OutputStream openOutputStream() throws IOException {
        return baos;
    }

    public byte[] getByteCode() {
        return baos.toByteArray();
    }
}

class MemJavaFileManager extends ForwardingJavaFileManager {
    private JavaCompiledCode javaCompiledCode;

    protected MemJavaFileManager(JavaFileManager fileManager, JavaCompiledCode javaCompiledCode) {
        super(fileManager);
        this.javaCompiledCode = javaCompiledCode;
    }

    @Override
    public JavaFileObject getJavaFileForOutput(Location location,
                                               String className,
                                               JavaFileObject.Kind kind,
                                               FileObject sibling) throws IOException {
        return javaCompiledCode;
    }
}

class MemClassLoader extends ClassLoader {
    private Map<String, JavaCompiledCode> compiledCode = new HashMap<>();

    public void setCode(JavaCompiledCode javaCompiledCode) {
        compiledCode.put(javaCompiledCode.getName(), javaCompiledCode);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        JavaCompiledCode javaCompiledCode = compiledCode.get(name);
        if (javaCompiledCode == null) {
            return super.findClass(name);
        }
        byte[] byteCode = javaCompiledCode.getByteCode();
        return defineClass(name, byteCode, 0, byteCode.length);
    }
}
