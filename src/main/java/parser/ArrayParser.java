package parser;

import java.util.Arrays;
import java.util.function.Function;

/**
 * parse array representation into array object
 * type: int
 * format: 1,2,3,4,5
 */
public class ArrayParser extends InputParser {
    public static Object[] parse(String input, Function parser) {
        String[] items = input.split(",");
        return Arrays.stream(items).map(parser).toArray();
    }

    public static int[] parseIntArray(String input) {
//        return parse(input, (String param) -> {
//            int value = Integer.parseInt(param);
//        });
        String[] items = input.split(",");
        return Arrays.stream(items).mapToInt(Integer::parseInt).toArray();
    }
}
