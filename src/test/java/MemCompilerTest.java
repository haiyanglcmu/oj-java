import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class MemCompilerTest {
    public String readJavaFile(String filename) throws IOException {
        File file = new File(filename);
        FileInputStream fis = new FileInputStream(file);
        long len = file.length();
        byte[] data = new byte[(int) len];
        fis.read(data);
        return new String(data);
    }

    @org.junit.Test
    public void testCompile() throws Exception {
        String source = readJavaFile("src/main/java/impl/BinarySearch.java");
        Class<?> clazz = Compiler.compile("impl.BinarySearch", source);

        String meta = "{" +
                "methodName: 'search'," +
                "returnType: 'int'," +
                "paramTypes: ['int[]', int]," +
                "target: -1" +
                "}";
        String tests = "[{input: ['1,2,3', '3'], expected: '1'}]";

        TestRunner.run(clazz, meta, tests);
    }
}