package parser;

public class InputParser {
    public static Object parse(String input, String paramType) {
        switch (paramType) {
            case "int":
                return Integer.parseInt(input);
            case "int[]":
                return ArrayParser.parseIntArray(input);
            default:
                return null;
        }
    }
}
