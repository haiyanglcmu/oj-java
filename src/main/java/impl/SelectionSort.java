package impl;

public class SelectionSort {
    public void sort(int[] array) {
        // select the maximum in every iteration
        for (int start = 0; start < array.length - 1; start++) {
            int min = start;
            for (int i = start + 1; i < array.length; i++) {
                if (array[i] < array[min]) {
                    min = i;
                }
            }
            swap(array, start, min);
        }
    }

    private void swap(int[] array, int i, int j) {
        int t = array[i];
        array[i] = array[j];
        array[j] = t;
    }
}
